const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost/nodejs_digitalocean_db', {
	useNewUrlParser: true,
	useUnifiedTopology:true
})
	.then(db => console.log(`database conectada a ${db.connection.host}`))
	.catch((err) => console.error(err));