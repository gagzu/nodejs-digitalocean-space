# NodeJs - DigitalOcean - Space

Este repositorio es un ejemplo practico de subida y obtención de imágenes de DigitalOcean Space. Una vez que se guardan las imágenes en DigitalOcean se almacena la url de la foto en mongodb para facilitar su obtención luego.

La app es muy básica y se puede mejorar y extender para hacer otras cosas geniales, siéntase libre de clonar el repositorio y mejorarlo a su gusto 😉

![GIF de la app en funcionamiento](https://gitlab.com/gagzu/nodejs-digitalocean-space/-/raw/master/src/assets/img/proceso-de-subida.gif)

## Variables de entorno

En el fichero .env.example podrán ver un ejemplo de las variables con datos ficticios como guía

* **PORT=** Puerto en el que corre la app
* **MOGODB_URI=** Url para la conexión de mongo
* **BUCKET_NAME=** Nombre de Space en DigitalOcean

**Nota:** DigitalOcean tiene una integración con AWS S3 por lo cual se puede usar su sdk, para mayor información de las variables de entorno requeridas para conectarse ir a la [documentación](https://aws.amazon.com/es/sdk-for-node-js/)

* **AWS_ACCESS_KEY_ID=** Clave publica para usar la API de DigitalOcean
* **AWS_SECRET_ACCESS_KEY=** Clave privada para usar la API de DigitalOcean
* **S3_ENDPOINT=** Url del bucket en DigitalOcean ejemplo: 'nyc3.digitaloceanspaces.com' (no se debe agregar el 'https://' ni tampoco el nombre del bucket)


#### Feliz codificación ✌️💻