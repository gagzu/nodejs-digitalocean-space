require('dotenv').config();
const app = require('./app');
require('./database');

app.listen(app.get('port'));
console.log('server on port', process.env.PORT);