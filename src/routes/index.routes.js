const path = require('path');
const { Router } = require('express');
const router = Router();

const {
	getFiles,
	uploadFile,
	renderIndex,
} = require(path.resolve(__dirname, '..', 'controllers', 'index.controllers'));

const { upload } = require(path.resolve(__dirname, '../libs/multer'));


router.get('/', renderIndex);

router.post('/upload', upload, uploadFile);

router.get('/files', getFiles);

module.exports = router;