const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

const { S3_ENDPOINT, BUCKET_NAME } = process.env;

const spaceEndpoint = new aws.Endpoint(S3_ENDPOINT);
const s3 = new aws.S3({
	endpoint: spaceEndpoint
})

const upload = multer({
	storage: multerS3({
		s3,
		acl:'public-read',
		bucket: BUCKET_NAME,
		metadata: (req, file, cb) => {
			console.log('metadata', file);
			cb(null, {
				fieldname: file.fieldname
			})
		},
		key: (req, file, cb) => {
			console.log('key', file);
			cb(null, file.originalname);
		}
	})
}).single('upload');

module.exports = { upload, s3 };