const path = require('path');
const morgan = require('morgan');
const express = require('express');
const app = express();

// Settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.resolve(__dirname, 'views'));
app.set('view engine', 'ejs');

// Mildwares
app.use(morgan('dev'));

// Static files
app.use(express.static(path.resolve(__dirname, 'public')));

// Routes
app.use(require(path.resolve(__dirname, 'routes', 'index.routes')));

module.exports = app;