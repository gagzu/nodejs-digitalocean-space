const Image = require('../models/Image');

const renderIndex = (req, res) => {
	res.render('upload', {
		title: 'Subir imágenes'
	})
};

const uploadFile = async (req, res) => {
	const newImage = new Image();
	newImage.url = req.file.location;

	await newImage.save();
	res.redirect('/files');
};

const getFiles = async (req, res) => {
	const images = await Image.find();
	console.log(images);
	res.render('files', {
		images,
		title: 'Listado de imágenes',
	})
};

const getFile = (req, res) => {};

module.exports = {
	getFile,
	getFiles,
	uploadFile,
	renderIndex,
}